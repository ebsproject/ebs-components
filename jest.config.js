module.exports = {
  testEnvironment: "jsdom",
  collectCoverageFrom: ["src/**/*.js"],
  transform: {
    "^.+\\.(j|t)sx?$": "babel-jest",
    ".+\\.(css|scss|png|jpg|svg)$": "jest-transform-stub",
  },
  coverageDirectory: "coverage/",
  coverageReporters: ["json","html"],
  moduleNameMapper: {
    "\\.(css)$": "identity-obj-proxy",
    "single-spa-react/parcel": "single-spa-react/lib/cjs/parcel.cjs",
  },
  setupFilesAfterEnv: ["@testing-library/jest-dom"],
};
