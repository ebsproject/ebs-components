ARG baseImage=ebsproject/cs-ebs-components
ARG baseImageTag=dev

FROM ${baseImage}:${baseImageTag}

LABEL maintainer="Kenichii A. Ana <k.ana@irri.org> & Renee Arianne F. Lat <r.lat@irri.org>"

ARG sourceDir=dist
ARG libVersion=xxxx
ARG libMainFile=ebs-components.js
ARG nginxDir=/usr/share/nginx/html
ARG latestDir=dev
ARG importMapFilename=app.importmap

RUN apt-get update && apt install -y jq

COPY ${sourceDir}/ ${nginxDir}/${libVersion}/

# Update import map with the module URL
RUN moduleName=${libVersion}; \
    moduleUrl=${libVersion}/${libMainFile}; \
    echo $(cat ${nginxDir}/${importMapFilename} | \
    jq --arg moduleName "$moduleName" \ 
    --arg moduleUrl "$moduleUrl" '.imports[$moduleName] = $moduleUrl') > ${nginxDir}/app.importmap && \
    moduleName=${libVersion}/; \
    moduleUrl=${libVersion}/; \
    echo $(cat ${nginxDir}/${importMapFilename} | \
    jq --arg moduleName "$moduleName" \
    --arg moduleUrl "$moduleUrl" '.imports[$moduleName] = $moduleUrl') > ${nginxDir}/app.importmap && \
    cp -r ${nginxDir}/${libVersion}/. ${nginxDir}/${latestDir}