const path = require('path');
const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: 'ebs',
    projectName: 'components',
    webpackConfigEnv,
    argv,
  });

  defaultConfig.externals.push(
    /@material-ui\/core.*/,
    /@material-ui\/icons.*/,
    /@material-ui\/styles.*/,
  );

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    resolve: {
      alias: {
        components: path.resolve(__dirname, './src/components/'),
        functions: path.resolve(__dirname, './src/functions/'),
        utils: path.resolve(__dirname, './src/utils/'),
      },
    },
  });
};
