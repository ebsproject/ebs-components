import React from 'react';
import PropTypes from 'prop-types';
import {Core,Styles, Icons} from '@ebs/styleguide'
// CORE COMPONENTS AND ATOMS TO USE
import HiddenColumns from '../../Atoms/HiddenColumns';
const {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Slide,
  Typography,
} =Core;
const { makeStyles } =Styles;
const { Build, Close } =Icons;

const useStyles = makeStyles((theme) => ({
  button: {
    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    },
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    marginLeft: theme.spacing(1),
    borderRadius: 4,
  },
  icon: {
    rotate: '90deg',
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const UserPreferencesMolecule = React.forwardRef(({ ...rest }, ref) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    /* 
     @prop data-testid: Id to use inside userpreferences.test.js file.
     */
    <div ref={ref} data-testid={'UserPreferencesTestId'}>
      <IconButton
        className={classes.button}
        onClick={handleClickOpen}
        aria-label='preferences'
      >
        <Build className={classes.icon} />
      </IconButton>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        fullWidth
        maxWidth='md'
        keepMounted
        onClose={handleClose}
        aria-labelledby='alert-dialog-slide-title'
        aria-describedby='alert-dialog-slide-description'
      >
        <DialogTitle id='alert-dialog-slide-title'>
          <Typography color='secondary'>
            User Preferences
          </Typography>
          <IconButton
            aria-label='close'
            className={classes.closeButton}
            onClick={handleClose}
          >
            <Close />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          <HiddenColumns {...rest} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color='secondary'>
            Close
          </Button>
          {/* <Button onClick={handleClose} color='primary'>
            Apply
          </Button> */}
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
UserPreferencesMolecule.propTypes = {};
// Default properties
UserPreferencesMolecule.defaultProps = {};

export default UserPreferencesMolecule;
