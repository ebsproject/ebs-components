import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Core, Styles, Icons } from '@ebs/styleguide';
// CORE COMPONENTS AND ATOMS TO USE
import GlobalFilter from 'components/Atoms/GlobalFilter';
import UserPreferences from 'components/Molecules/UserPreferences';
import { transform } from 'node-json-transform';
import CSVExport from 'functions/csv';
const { GetApp } = Icons;
const { Box, IconButton } = Core;
const { makeStyles } = Styles;

const useStyles = makeStyles((theme) => ({
  button: {
    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    },
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    marginLeft: theme.spacing(1),
    borderRadius: 4,
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ToolbarMolecule = React.forwardRef(
  (
    {
      data,
      title,
      columns,
      toolbar,
      refreshTable,
      globalFilter,
      toolbarActions,
      csvFileName,
      setGlobalFilter,
      selectedFlatRows,
      ...rest
    },
    ref,
  ) => {
    const classes = useStyles();

    const csvExport = () => {
      let headers = {};
      let map = {};
      let formattedData = [];
      // * Building Map and Headers
      columns.map((column) => {
        if (!column.hidden) {
          map[column.accessor.replace('.', '')] = column.accessor;
          headers[column.accessor.replace('.', '')] = column.csvHeader;
        }
      });
      // * Building formattedData
      data.map((item) => {
        formattedData.push(transform(item, { item: map }));
      });
      // * Export Data to CSV
      CSVExport(headers, formattedData, csvFileName);
    };

    return (
      /* 
     @prop data-testid: Id to use inside toolbar.test.js file.
     */
      <div ref={ref} data-testid={'ToolbarTestId'}>
        <Box display='flex' alignItems='center'>
          <Box flexGrow={1}>{title}</Box>
        </Box>
        {toolbar && (
          <Fragment>
            <Box display='flex' alignItems='center' flexDirection='row-reverse'>
              <Box>
                <UserPreferences {...rest} />
              </Box>
              <Box>
                <IconButton
                  className={classes.button}
                  onClick={csvExport}
                  color='secondary'
                  aria-label='download file'
                >
                  <GetApp />
                </IconButton>
              </Box>
              {toolbarActions && (
                <Box>
                  {toolbarActions(
                    selectedFlatRows.map((d) => d.original),
                    refreshTable,
                  )}
                </Box>
              )}
              <Box>
                <GlobalFilter
                  globalFilter={globalFilter}
                  setGlobalFilter={setGlobalFilter}
                />
              </Box>
            </Box>
          </Fragment>
        )}
      </div>
    );
  },
);
// Type and required properties
ToolbarMolecule.propTypes = {
  title: PropTypes.node,
  toolbar: PropTypes.bool.isRequired,
  refreshTable: PropTypes.func,
  globalFilter: PropTypes.any,
  toolbarActions: PropTypes.func,
  setGlobalFilter: PropTypes.func.isRequired,
  selectedFlatRows: PropTypes.array.isRequired,
};
// Default properties
ToolbarMolecule.defaultProps = {
  toolbar: true,
};

export default ToolbarMolecule;
