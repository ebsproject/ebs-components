import React from 'react';
import PropTypes from 'prop-types';
import {Core, Styles, Icons} from '@ebs/styleguide'
// CORE COMPONENTS AND ATOMS TO USE
const {
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
} =Core;
const { withStyles } =Styles;
const {
  ArrowDownward,
  ArrowUpward,
  FormatIndentDecrease,
  FormatIndentIncrease,
  MoreVert,
  SortByAlpha,
} =Icons;

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const MenuColumnMolecule = React.forwardRef(({ column }, ref) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    /* 
     @prop data-testid: Id to use inside menucolumn.test.js file.
     */
    <div data-testid={'MenuColumnTestId'}>
      <IconButton onClick={handleClick} size='small' color='primary'>
        <MoreVert />
      </IconButton>
      <StyledMenu
        id='customized-menu'
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {/* // * If the column can be grouped, let's add a toggle */}
        {column.canGroupBy && (
          <StyledMenuItem {...column.getGroupByToggleProps()}>
            <ListItemIcon>
              {column.isGrouped ? (
                <FormatIndentDecrease />
              ) : (
                <FormatIndentIncrease />
              )}
            </ListItemIcon>
            <ListItemText primary='Group by' />
          </StyledMenuItem>
        )}
        {/* // * If the column can be shorted, let's add a toggle */}
        {column.canSort && (
          <StyledMenuItem {...column.getSortByToggleProps()}>
            <ListItemIcon>
              {column.isSorted ? (
                column.isSortedDesc ? (
                  <ArrowUpward />
                ) : (
                  <ArrowDownward />
                )
              ) : (
                <SortByAlpha />
              )}
            </ListItemIcon>
            <ListItemText primary='Sort by' />
          </StyledMenuItem>
        )}
      </StyledMenu>
    </div>
  );
});
// Type and required properties
MenuColumnMolecule.propTypes = {
  column: PropTypes.object.isRequired,
};
// Default properties
MenuColumnMolecule.defaultProps = {};

export default MenuColumnMolecule;
