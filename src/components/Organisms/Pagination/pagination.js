import React from 'react';
import PropTypes from 'prop-types';
import {Core} from '@ebs/styleguide'
// CORE COMPONENTS AND MOLECULES TO USE
import PaginationActions from 'components/Molecules/PaginationActions';
const { Box, TextField, Typography,TablePagination } =Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PaginationOrganism = React.forwardRef(
  (
    {
      pageIndex,
      pageSize,
      setPageSize,
      gotoPage,
      totalElements,
      pageCount,
    },
    ref,
  ) => {

    const handleChangePage = (event, newPage) => {
      gotoPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
      setPageSize(parseInt(event.target.value, 10));
      gotoPage(0);
    };

    return (
      /* 
   @prop data-testid: Id to use inside pagination.test.js file.
   */
      <div data-testid={'PaginationTestId'} ref={ref}>
        <Box
          display='flex'
          flexWrap='nowrap'
          flexDirection='row'
          alignItems='center'
        >
          <Box>
            <Typography component='span' variant='body1'>
              Go to page:{' '}
            </Typography>
          </Box>
          <Box>
            <TextField
              component='span'
              size='small'
              variant='outlined'
              type='number'
              color='secondary'
              defaultValue={pageIndex + 1}
              onChange={(e) => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                gotoPage(page);
              }}
              style={{ width: '70px' }}
            />
          </Box>
          <Box>
            <Typography component='span' variant='h3'>
              <TablePagination
                rowsPerPageOptions={[10, 25, 50, 100]}
                component='div'
                count={totalElements}
                rowsPerPage={pageSize}
                page={pageIndex}
                labelDisplayedRows={({ from, to, count }) => (
                  <Typography variant='body1'>
                    {from}-{to} of {count != -1 ? count : `more than ${count}`}
                  </Typography>
                )}
                labelRowsPerPage={
                  <Typography variant='body1'>Rows Per Page:</Typography>
                }
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                ActionsComponent={(props) => (
                  <PaginationActions {...props} pageCount={pageCount} />
                )}
              />
            </Typography>
          </Box>
        </Box>
      </div>
    );
  },
);
// Type and required properties
PaginationOrganism.propTypes = {
  pageIndex: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  setPageSize: PropTypes.func.isRequired,
  gotoPage: PropTypes.func.isRequired,
};
// Default properties
PaginationOrganism.defaultProps = {};

export default PaginationOrganism;
