import React from "react";
import PropTypes from "prop-types";
import {Core} from '@ebs/styleguide'
// CORE COMPONENTS
import { useAsyncDebounce } from "react-table";
const { TextField }=Core;

//MAIN FUNCTION
/*
 @prop column: properties to handle filters
 @param ref: reference made by React.forward
*/
const DefaultFilterAtom = React.forwardRef(
  ({ column: { filterValue, setFilter } }, ref) => {
    const [value, setValue] = React.useState(filterValue);
    const onChange = useAsyncDebounce((value) => {
      setFilter(value || undefined);
    }, 300);
    return (
      /* 
     @prop data-testid: Id to use inside defaultfilter.test.js file.
     */
      <div data-testid={"DefaultFilterTestId"} ref={ref}>
        <TextField
          fullWidth
          variant="standard"
          value={value || ""}
          onChange={(e) => {
            setValue(e.target.value);
            onChange(e.target.value); // * Set undefined to remove the filter entirely
          }}
        />
      </div>
    );
  }
);
// * Type and required properties
DefaultFilterAtom.propTypes = {
  filterValue: PropTypes.string,
  setFilter: PropTypes.func.isRequired,
};
// * Default properties
DefaultFilterAtom.defaultProps = {
  filterValue: "",
};

export default DefaultFilterAtom;
