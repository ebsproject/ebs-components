import React from "react";
import PropTypes from "prop-types";
import {Core} from '@ebs/styleguide'
// CORE COMPONENTS
const { Box, Checkbox, Grid, Typography } =Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SelectAtom = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return (
      /* 
     @prop data-testid: Id to use inside select.test.js file.
     */
      <div data-testid={"SelectTestId"} ref={ref}>
        <Checkbox ref={resolvedRef} {...rest} />
      </div>
    );
  }
);
// Type and required properties
SelectAtom.propTypes = {
  indeterminate: PropTypes.bool,
};
// Default properties
SelectAtom.defaultProps = {};

export default SelectAtom;
