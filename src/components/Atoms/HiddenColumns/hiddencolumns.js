import React from "react";
import PropTypes from "prop-types";
import {Core, Styles, Icons} from '@ebs/styleguide'
// CORE COMPONENTS
import Select from "../Select";
const { Box, List, ListSubheader, Typography } =Core;
const { makeStyles } =Styles;
const { Forward, Visibility, VisibilityOff } =Icons;

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  list: {
    height: "40vh",
    backgroundColor: theme.palette.background.default,
    position: "relative",
    overflow: "auto",
  },
  icon: {
    fontSize: 60,
    color: theme.palette.text.hint,
  },
  iconRotate: {
    fontSize: 60,
    rotate: "-180deg",
    color: theme.palette.text.hint,
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const HiddenColumnsAtom = React.forwardRef(({ allColumns }, ref) => {
  const classes = useStyles();

  return (
    /* 
     @prop data-testid: Id to use inside hiddencolumns.test.js file.
     */
    <div data-testid={"HiddenColumnsTestId"} ref={ref} className={classes.root}>
      <Box display="flex" alignItems="center">
        <Box flexGrow={1}>
          <List
            className={classes.list}
            subheader={
              <ListSubheader>
                <Typography variant="h4" color="secondary">
                  Displayed Columns
                </Typography>
              </ListSubheader>
            }
          >
            {allColumns
              .filter((column) => column.isVisible)
              .map((column, key) => (
                <Box
                  display="flex"
                  justifyContent="flex-start"
                  flexDirection="row"
                  alignItems="center"
                  key={key}
                >
                  <Box>
                    <Select
                      checkedIcon={<Visibility />}
                      icon={<VisibilityOff />}
                      {...column.getToggleHiddenProps()}
                    />
                  </Box>
                  <Box>
                    {column.accessor ? (
                      column.Header
                    ) : (
                      <Typography variant="h5" color="primary">
                        {column.id}
                      </Typography>
                    )}
                  </Box>
                </Box>
              ))}
          </List>
        </Box>
        <Box>
          <Forward className={classes.iconRotate} />
          <Forward className={classes.icon} />
        </Box>
        <Box flexGrow={1}>
          <List
            className={classes.list}
            subheader={
              <ListSubheader>
                <Typography variant="h4" color="secondary">
                  Hidden Columns
                </Typography>
              </ListSubheader>
            }
          >
            {allColumns
              .filter((column) => {
                if (!column.isVisible) {
                  if (!column.hidden) {
                    return column;
                  }
                }
              })
              .map((column, key) => (
                <Box
                  display="flex"
                  justifyContent="flex-start"
                  flexDirection="row"
                  alignItems="center"
                  key={key}
                >
                  <Box>
                    <Select
                      checkedIcon={<Visibility />}
                      icon={<VisibilityOff />}
                      {...column.getToggleHiddenProps()}
                    />
                  </Box>
                  <Box>
                    {column.accessor ? (
                      column.Header
                    ) : (
                      <Typography variant="h5" color="primary">
                        {column.id}
                      </Typography>
                    )}
                  </Box>
                </Box>
              ))}
          </List>
        </Box>
      </Box>
    </div>
  );
});
// Type and required properties
HiddenColumnsAtom.propTypes = {};
// Default properties
HiddenColumnsAtom.defaultProps = {};

export default HiddenColumnsAtom;
