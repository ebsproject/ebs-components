import React from "react";
import PropTypes from "prop-types";
import {Core, Styles,Icons} from '@ebs/styleguide'
// CORE COMPONENTS
const { makeStyles }=Styles;
const { Divider, IconButton, InputBase, Paper }=Core;
const { Clear, Search } =Icons;
// Global filter styles
const useStyles = makeStyles((theme) => ({
  root: {
    padding: "2px 8px",
    display: "flex",
    alignItems: "center",
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const GlobalFilterAtom = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const { globalFilter, setGlobalFilter } = props;
  const [value, setValue] = React.useState(globalFilter);
  const search = (value) => {
    setGlobalFilter(value);
  };

  return (
    /* 
     @prop data-testid: Id to use inside globalfilter.test.js file.
     */
    <div data-testid={"GlobalFilterTestId"} ref={ref}>
      <Paper className={classes.root}>
        <InputBase
          value={value || ""}
          color="primary"
          onChange={(e) => {
            setValue(e.target.value);
          }}
          className={classes.input}
          placeholder="Search..."
          inputProps={{ "aria-label": "search..." }}
        />
        <IconButton
          onClick={() => search(value)}
          color="primary"
          className={classes.iconButton}
          aria-label="clear"
        >
          <Search />
        </IconButton>
        <Divider className={classes.divider} orientation="vertical" />
        <IconButton
          onClick={(e) => {
            setValue("");
            search(undefined);
          }}
          color="secondary"
          className={classes.iconButton}
          aria-label="clear"
        >
          <Clear />
        </IconButton>
      </Paper>
    </div>
  );
});
// Type and required properties
GlobalFilterAtom.propTypes = {
  globalFilter: PropTypes.string,
  setGlobalFilter: PropTypes.func.isRequired,
};
// Default properties
GlobalFilterAtom.defaultProps = {
  globalFilter: "",
};

export default GlobalFilterAtom;
