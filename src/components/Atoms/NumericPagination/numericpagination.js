import React from 'react';
import PropTypes from 'prop-types';
import {Core} from '@ebs/styleguide'
// CORE COMPONENTS
const { IconButton, Typography } =Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const NumericPaginationAtom = React.forwardRef(
  ({ page, onPageChange, pageCount }, ref) => {
    const delta = 2;
    let range = [];
    let rangeWithDots = [];
    let l;

    range.push(l);

    const handleChange = (e, newPage) => {
      onPageChange(e, newPage);
    };

    if (pageCount <= 1) {
      return range.map((number, key) => (
        <IconButton
          key={key}
          color={page === key ? 'primary' : 'default'}
          onClick={(e) => handleChange(e, key)}
        >
          <Typography variant='body1'>{key + 1}</Typography>
        </IconButton>
      ));
    }

    for (let i = page - delta; i <= page + delta; i++) {
      if (i < pageCount && i > 1) {
        range.push(i);
      }
    }
    range.push(pageCount);
    for (let i of range) {
      if (l) {
        if (i - l === 2) {
          rangeWithDots.push(l + 1);
        } else if (i - l !== 1) {
          rangeWithDots.push('...');
        }
      }
      rangeWithDots.push(i);
      l = i;
    }
    return rangeWithDots.map((number, key) => (
      <IconButton
        key={key}
        color={page === key ? 'primary' : 'default'}
        onClick={(e) => (number === '...' ? null : handleChange(e, number - 1))}
      >
        <Typography variant='body1'>{number}</Typography>
      </IconButton>
    ));
  },
);
// Type and required properties
NumericPaginationAtom.propTypes = {
  page: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  pageCount: PropTypes.number.isRequired,
};
// Default properties
NumericPaginationAtom.defaultProps = {};

export default NumericPaginationAtom;
