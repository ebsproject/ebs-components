import React from 'react'
import PropTypes from 'prop-types'
import {Core, Styles} from '@ebs/styleguide'
// CORE COMPONENTS
const {LinearProgress} =Core
// STYLES
const { makeStyles } =Styles;

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}))
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ProgressAtom = React.forwardRef((props, ref) => {
  const classes = useStyles()

  return (
    /*
     @prop data-testid: Id to use inside progress.test.js file.
     */
    <div className={classes.root} data-testid={'ProgressTestId'}>
      <LinearProgress />
    </div>
  )
})
// Type and required properties
ProgressAtom.propTypes = {
}
// Default properties
ProgressAtom.defaultProps = {
}

export default ProgressAtom
