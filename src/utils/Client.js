import { client as Graphql } from './Apollo';
import { ClientAxios } from './Axios';
import { buildQuery } from '../functions/query';

// * Function to extract header columns
export function extractColumns(headers) {
  let columns = [];
  headers.map((header) => {
    header.accessor
      ? columns.push(header)
      : header.columns.map((header) => {
          columns.push(header);
        });
  });
  return columns;
}
// * Function to call different clients (Brapi or Graphql)
// @param uri: api endpoint
// @param page: Query parameter to handle page number
// @param sort: Query parameter to handle sorting columns
// @param entity: Api Entity
// @param filters: Query parameter to handle filters
// @param columns: Columns extracted to handle content from API
// @param callStandard: Brapi or Graphql
export default async function Client({
  uri,
  page,
  sort,
  entity,
  filters,
  columns,
  callStandard,
}) {
  return new Promise((resolve, reject) => {
    // ? Brapi or Graphql standard
    switch (callStandard.toLowerCase()) {
      case 'brapi':
        ClientAxios(uri, entity, query, page, sort, filters)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
        break;
      case 'graphql':
        const QUERY = buildQuery(entity, columns);
        Graphql(uri)
          .query({
            query: QUERY,
            variables: {
              page: page,
              sort: sort,
              filters: filters,
              disjunctionFilters: filters.length > 1 ? true : false,
            },
            fetchPolicy: 'no-cache',
          })
          .then(({ data }) => {
            let payload = {
              data: data[`find${entity}List`].content,
              pages: data[`find${entity}List`].totalPages,
              elements: data[`find${entity}List`].totalElements,
            };
            resolve(payload);
          })
          .catch((error) => reject(error));
        break;
      default:
        return;
    }
  });
}
