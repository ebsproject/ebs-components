import { gql } from '@apollo/client';
export const buildQuery = (entity, apiContent) => {
  return gql`
    query find${entity}List($page: PageInput, $sort: [SortInput], $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            content {
                ${apiContent.map((item) => {
                  const levels = item.accessor.split('.');
                  // Setting levels
                  return extractLevels(levels);
                })}
            }
        }
    }`;
};

function extractLevels(arr) {
  let levels = arr;
  if (levels.length > 1) {
    return `${levels.shift()}{
        ${extractLevels(levels)}
    }`;
  } else {
    return `${levels[0]}`;
  }
}
