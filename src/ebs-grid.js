import React, { useCallback, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import Client from 'utils/Client';
import MainTable from './components/Organisms/MainTable';
import { extractColumns } from 'utils/Client';
import 'regenerator-runtime/runtime';

// @param uri: endpoint Uri
// @param title: Table title
// @param select: Component or kind of select implemented
// @param entity: entity to handle
// @param height: table height
// @param indexing: table indexing
// @param rowactions: Component to add on every row as Actions
// @param callstandar: Brapi/Graphql standards to handle
// @param toolbaractions: Component to add on top table as Global actions
// @param detailcomponent: Component used as master detail on every row
export const EbsGrid = React.forwardRef(
  (
    {
      columns: originalColumns,
      uri,
      title,
      fetch,
      select,
      entity,
      height,
      toolbar,
      indexing,
      rowactions,
      csvfilename,
      callstandard,
      toolbaractions,
      detailcomponent,
    },
    ref,
  ) => {
    const columns = useMemo(() => originalColumns, []);
    // * We'll start our table without any data
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [pageCount, setPageCount] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const fetchIdRef = React.useRef(0);
    const extractedColumns = extractColumns(originalColumns);
    // * Fetch our data function
    const fetchData = useCallback(
      ({ pageSize, pageIndex, sortBy, filters, globalFilter }) => {
        let Filters = [];
        const page = { number: pageIndex + 1, size: pageSize };
        const sort = [{ col: 'id', mod: 'DES' }];
        sortBy.map((column) => {
          sort.push({ col: column.id, mod: column.desc ? 'DES' : 'ASC' });
        });
        // * This will get called when the table needs new data
        // * Give this fetch an ID
        const fetchId = ++fetchIdRef.current;

        // * Set the loading state
        setLoading(true);

        // * Setting up filters
        filters.map((filter) =>
          Filters.push({ col: filter.id, mod: 'LK', val: filter.value }),
        );
        // ? Global filter ?
        globalFilter &&
          extractedColumns.map((column) => {
            if (!column.disableGlobalFilter) {
              Filters.push({
                col: column.accessor,
                mod: 'LK',
                val: globalFilter,
              });
            }
          });
        // * Only update the data if this is the latest fetch
        if (fetchId === fetchIdRef.current) {
          // * Fetching from custom data
          if (fetch) {
            fetch({ page: page, sort: sort, filters: filters })
              .then(({ pages, data, elements }) => {
                setPageCount(pages);
                setLoading(false);
                setData(data);
                setTotalElements(elements);
              })
              .catch(({ message }) => {
                console.log(message);
              });
          } else {
            Client({
              uri: uri,
              page: page,
              sort: sort,
              entity: entity,
              filters: Filters,
              columns: extractedColumns,
              callStandard: callstandard,
            })
              .then(({ pages, data, elements }) => {
                setPageCount(pages);
                setLoading(false);
                setData(data);
                setTotalElements(elements);
              })
              .catch(({ message }) => {
                console.log(message);
              });
          }
        }
      },
      [],
    );

    return (
      <MainTable
        data={data}
        title={title}
        height={height}
        select={select}
        columns={columns}
        loading={loading}
        toolbar={toolbar}
        indexing={indexing}
        fetchData={fetchData}
        pageCount={pageCount}
        rowActions={rowactions}
        csvFileName={csvfilename}
        totalElements={totalElements}
        toolbarActions={toolbaractions}
        renderRowSubComponent={detailcomponent}
      />
    );
  },
);

MainTable.propTypes = {
  uri: PropTypes.string,
  title: PropTypes.node,
  select: PropTypes.any,
  entity: PropTypes.string,
  height: PropTypes.any.isRequired,
  columns: PropTypes.array.isRequired,
  toolbar: PropTypes.bool,
  indexing: PropTypes.bool,
  rowactions: PropTypes.func,
  callstandard: PropTypes.string,
  toolbaractions: PropTypes.func,
  detailcomponent: PropTypes.func,
};
